<?php

namespace Wif\Mailjet;

use Illuminate\Mail\Transport\Transport;
use Mailjet\Client as MailjetClient;
use Mailjet\Resources;
use Swift_Mime_Message;

/**
 * Class MailjetTransport
 * @package Wif\Mailjet
 */
class MailjetTransport extends Transport
{

    /**
     * @var MailjetClient
     */
    private $mailjetClient;

    /**
     * MailjetTransport constructor.
     * @param MailjetClient $client
     */
    public function __construct(MailjetClient $client)
    {
        $this->mailjetClient = $client;
    }

    /**
     * Send the given Message.
     *
     * Recipient/sender data will be retrieved from the Message API.
     * The return value is the number of recipients who were accepted for delivery.
     *
     * @param Swift_Mime_Message $message
     * @param string[] $failedRecipients An array of failures by-reference
     *
     * @return int
     */
    public function send(Swift_Mime_Message $message, &$failedRecipients = null)
    {
        $this->beforeSendPerformed($message);

        $this->mailjetClient->post(Resources::$Email, [
            'body' => $this->payload($message)
        ]);

        $this->sendPerformed($message);

        return $this->numberOfRecipients($message);
    }

    /**
     * @param Swift_Mime_Message $message
     * @return array
     */
    protected function payload(Swift_Mime_Message $message): array
    {
        $data = [];

        $this->withRecipients($data, $message);

        foreach ($message->getTo() as $address => $display) {
            $data['Recipients'][] = ['Email' => $address, 'Name' => $display];
        }

        $data['Subject'] = $message->getSubject();

        $this->withMailBody($data, $message);

        return $data;
    }

    /**
     * @param array $data
     * @param Swift_Mime_Message $message
     */
    protected function withMailBody(array & $data, Swift_Mime_Message $message): void
    {
        if ($message->getContentType() == 'text/html') {
            $data['Html-Part'] = $message->getBody();
        } elseif ($message->getContentType() == 'text/plain') {
            $data['Text-Part'] = $message->getBody();
        }
    }

    /**
     * @param array $data
     * @param Swift_Mime_Message $message
     */
    protected function withRecipients(array & $data, Swift_Mime_Message $message): void
    {
        $from = $message->getFrom();

        $fromEmail = array_keys($from);
        $fromEmail = reset($fromEmail);

        $data['FromEmail'] = $fromEmail;
        $data['FromName'] = $from[$fromEmail];
    }
}