<?php

namespace Wif\Mailjet;

use Illuminate\Foundation\Application;
use Illuminate\Mail\TransportManager;
use Illuminate\Support\ServiceProvider;
use Mailjet\Client;

/**
 * Class MailjetMailServiceProvider
 * @package Wif\Mailjet
 */
class MailjetMailServiceProvider extends ServiceProvider
{

    /**
     * @var bool
     */
    protected $defer = false;

    /**
     * @return void
     */
    public function register(): void
    {
        $this->publishes([
            __DIR__ . '/../configs/mailjet.php' => config_path('mailjet.php')
        ]);

        $this->app->bind(Client::class, function (Application $app) {
            return new Client($app['config']['mailjet.api-public'], $app['config']['mailjet.api-private']);
        });

        $this->app->resolving('swift.transport', function (TransportManager $manager) {
            $manager->extend('mailjet', function (Application $app) {
                $client = $app->make(Client::class);

                return new MailjetTransport($client);
            });
        });
    }

    /**
     * @return array
     */
    public function provides(): array
    {
        return ['swift.transport', Client::class];
    }
}