<?php

return [
    'api-public' => env('MAILJET_PUBLIC', ''),
    'api-private' => env('MAILJET_PRIVATE', '')
];